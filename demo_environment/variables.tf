data "aws_ami" "windows" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["Windows_Server-2019-English-Full-Base-20*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_ami" "al2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

variable "vpc_cidr" { default = "192.168.100.0/24" }
variable "subnet_cidr" { default = "192.168.100.0/24" }
variable "remote_source_ip" { }

data "template_file" "vault_userdata" {
  template = file("./userdata.sh")
  vars = {
    role_name = aws_iam_role.example_client_instance_role.name
  }
}


data "template_file" "windows_userdata" {
  template = file("./windows_userdata.ps1")
  vars = {
    vault_ip = module.vault_ec2.private_ip[0] 
  }
}

output "windows_ec2_ip" {
 value = module.windows_ec2.public_ip 
}


output "vault_ec2_ip" {
 value = module.vault_ec2.public_ip 
}