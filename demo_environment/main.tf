module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "fcal-vault-demo"
  cidr = var.vpc_cidr

  azs            = ["eu-west-2a"]
  public_subnets = [var.subnet_cidr]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "windows_ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "windows-ec2"
  instance_count = 1

  ami                    = data.aws_ami.windows.id
  instance_type          = "t3a.small"
  key_name               = "t420"
  monitoring             = true
  vpc_security_group_ids = [module.rdp_test_sg.this_security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  user_data = data.template_file.windows_userdata.rendered

  iam_instance_profile = aws_iam_instance_profile.example_instance_profile.name

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "vault_ec2" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name           = "vault-ec2"
  instance_count = 1

  ami                    = data.aws_ami.al2.id
  instance_type          = "t2.micro"
  key_name               = "t420"
  monitoring             = true
  vpc_security_group_ids = [module.vault_sg.this_security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  user_data = data.template_file.vault_userdata.rendered

  iam_instance_profile = aws_iam_instance_profile.example_instance_profile.name

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "rdp_test_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "rdp-test"
  description = "Security group for test RDP host"
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 3389
      to_port     = 3389
      protocol    = "tcp"
      description = "RDP Ports"
      cidr_blocks = var.remote_source_ip
    }
  ]

  egress_with_source_security_group_id = [
    {
      from_port   = 8200
      to_port     = 8201
      protocol    = "tcp"
      description = "Vault User Ports"
      source_security_group_id = module.vault_sg.this_security_group_id
    }
  ]

  egress_rules = ["https-443-tcp", "http-80-tcp"]
  
}

module "vault_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "vault-clients"
  description = "Security group for Vault clients"
  vpc_id      = module.vpc.vpc_id

  ingress_with_source_security_group_id = [
    {
      from_port   = 8200
      to_port     = 8201
      protocol    = "tcp"
      description = "Vault User Ports"
      source_security_group_id = module.rdp_test_sg.this_security_group_id
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      description = "CA Cert Distribution"
      source_security_group_id = module.rdp_test_sg.this_security_group_id
    }
  ]

    ingress_with_cidr_blocks = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      description = "ssh mgmt"
      cidr_blocks = var.remote_source_ip
    }
  ]

  egress_rules = ["https-443-tcp", "http-80-tcp"]

}

resource "aws_iam_role_policy" "vault_iam" {
  name   = "vault_iam"
  role   = aws_iam_role.example_client_instance_role.id
  policy = data.aws_iam_policy_document.vault_iam.json
}

data "aws_iam_policy_document" "vault_iam" {
  statement {
    effect  = "Allow"
    actions = ["iam:GetRole", "iam:GetUser", "iam:GetInstanceProfile"]   
    resources = [
      "arn:aws:iam::*:user/*",
      "arn:aws:iam::*:role/Vault*",
    ]
  }
  statement {
    effect    = "Allow"
    actions   = ["sts:GetCallerIdentity"]
    resources = ["*"]
  }
}

data "aws_iam_policy_document" "example_instance_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "example_client_instance_role" {
  name_prefix        = "Vault-RDP-Enrolment"
  assume_role_policy = data.aws_iam_policy_document.example_instance_role.json
}

resource "aws_iam_instance_profile" "example_instance_profile" {
  path = "/"
  role = aws_iam_role.example_client_instance_role.name
}
