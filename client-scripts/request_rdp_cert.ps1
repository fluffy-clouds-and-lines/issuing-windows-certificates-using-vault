Start-Transcript -OutputDirectory "${env:SYSTEMROOT}\vault_rdp_sign\transcripts\"

add-type @"
    using System.Net;
    using System.Security.Cryptography.X509Certificates;
    public class TrustAllCertsPolicy : ICertificatePolicy {
        public bool CheckValidationResult(
            ServicePoint srvPoint, X509Certificate certificate,
            WebRequest request, int certificateProblem) {
            return true;
        }
    }
"@
[System.Net.ServicePointManager]::CertificatePolicy = New-Object TrustAllCertsPolicy

Invoke-WebRequest -Uri "${env:VAULT_ADDR}/v1/pki/ca/pem" -OutFile "${env:TEMP}\ca_cert.cer"

Import-Certificate -FilePath "${env:TEMP}\ca_cert.cer"-CertStoreLocation Cert:\LocalMachine\Root

$fqdn = ([System.Net.Dns]::GetHostByName(($env:computerName))).Hostname
$csr_filename = -join ((65..90) + (97..122) | Get-Random -Count 15 | % {[char]$_})

$ini_file = Get-Content -Path "${env:SYSTEMROOT}\vault_rdp_sign\rdp-certificate.ini.tmpl"
$ini_file -Replace 'TMPL_HOSTNAME',$fqdn | Set-Content -Path "${env:SYSTEMROOT}\vault_rdp_sign\rdp-certificate.ini"

certreq -new "${env:SYSTEMROOT}\vault_rdp_sign\rdp-certificate.ini" "${env:TEMP}\${csr_filename}.csr"

vault login -method=aws header_value=vault-demo.infra.fluffycloudsandlines.blog role=rdp-issue-iam

$sign_response = $(vault write pki/sign/rdp-cert csr=@"${env:TEMP}\${csr_filename}.csr")

$cert_json = ($sign_response | ConvertFrom-Json)

Set-Content -Path .\signed_cert.crt -Value $cert_json.data.certificate

$cert_import_response = Import-Certificate -FilePath .\signed_cert.crt -CertStoreLocation Cert:\LocalMachine\My

$cert_thumbprint = $cert_import_response.Thumbprint

wmic /namespace:\\root\CIMV2\TerminalServices PATH Win32_TSGeneralSetting Set SSLCertificateSHA1Hash="${cert_thumbprint}"