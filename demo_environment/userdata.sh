#!/usr/bin/env bash
set -xe

# Install nginx to serve our Root CA Cert - naughty!
amazon-linux-extras install -y nginx1
yum update -y

# Grab terraform-aws-vault and 'install' vault (systemd etc)
cd /tmp
wget https://github.com/hashicorp/terraform-aws-vault/archive/v0.13.7.zip
unzip ./v0.13.7.zip
rm -f ./v0.13.7.zip
./terraform-aws-vault-0.13.7/modules/install-vault/install-vault --version 1.4.3
wget https://releases.hashicorp.com/terraform/0.12.28/terraform_0.12.28_linux_amd64.zip
unzip terraform_0.12.28_linux_amd64.zip
mv terraform /usr/local/sbin/terraform

# Generate our self-signed root CA certs and Vault Server cert / keypair
cd ./terraform-aws-vault-0.13.7/modules/private-tls-cert/
terraform init
terraform apply -var ca_public_key_file_path=/opt/vault/tls/ca.crt.pem \
                -var public_key_file_path=/opt/vault/tls/vault.crt.pem \
                -var private_key_file_path=/opt/vault/tls/vault.key.pem \
                -var owner=vault \
                -var organization_name='FCAL' \
                -var ca_common_name=infra.fluffycloudsandlines.blog \
                -var common_name=vault-demo.infra.fluffycloudsandlines.blog \
                -var dns_names='["vault-demo.infra.fluffycloudsandlines.blog"]' \
                -var ip_addresses='["127.0.0.1"]'\
                -var validity_period_hours=24 \
                -auto-approve

# Add the Root CA into the OS trust store
cd /tmp
./terraform-aws-vault-0.13.7/modules/update-certificate-store/update-certificate-store --cert-file-path /opt/vault/tls/ca.crt.pem

# Run the Vault Config script
rm -f /opt/vault/config/default.hcl
/opt/vault/bin/run-vault --tls-cert-file /opt/vault/tls/vault.crt.pem --tls-key-file /opt/vault/tls/vault.key.pem

# Remove consul storage backend
sed -i -e '/storage "consul"/,/}/d' /opt/vault/config/default.hcl

# Add local storage backend
cat << EOF > /opt/vault/config/fcal.hcl
storage "file" {
  path = "/opt/vault/data"
}
EOF

systemctl restart vault.service 

# Wait for vualt boot then initialise, and capture unseal keys - naughty, again
sleep 3
/opt/vault/bin/vault operator init > /opt/vault/config/init_output

export PATH=$PATH:/opt/vault/bin
export VAULT_ROOT_KEY=$(cat /opt/vault/config/init_output | grep "Root Token" | cut -d ":" -f 2 | xargs)
export VAULT_UK1_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 1" | cut -d ":" -f 2 | xargs)
export VAULT_UK2_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 2" | cut -d ":" -f 2 | xargs)
export VAULT_UK3_KEY=$(cat /opt/vault/config/init_output | grep "Unseal Key 3" | cut -d ":" -f 2 | xargs)
export WINDOWS_DOMAIN=win.infra.fluffycloudsandlines.blog
export VAULT_FORMAT='json'

export

# Unseal our vault ready for setup
sleep 3
vault operator unseal $VAULT_UK1_KEY
vault operator unseal $VAULT_UK2_KEY
vault operator unseal $VAULT_UK3_KEY
vault login $VAULT_ROOT_KEY

# Enable the pki engine on default path /pki
vault secrets enable pki
vault secrets tune -max-lease-ttl=8760h pki

# setup our Vault mastered CA. In the realworld this would be a 
# sub-ca of an external Root CA with very tight controls
vault write pki/root/generate/internal \
    common_name=$WINDOWS_DOMAIN \
    ttl=8760h

vault write pki/config/urls \
    issuing_certificates="http://vault-demo.infra.fluffycloudsandlines.blog:8200/v1/pki/ca" \
    crl_distribution_points="http://vault-demo.infra.fluffycloudsandlines.blog:8200/v1/pki/crl"

# Setup a specific rdp certificate configuration, all CSRs are checked against these criteria,
# and certs issued from this endpoint will have these attributes applied
vault write pki/roles/rdp-cert \
    allowed_domains=$WINDOWS_DOMAIN \
    allow_subdomains=true \
    max_ttl=72h \
    ext_key_usage_oids=1.3.6.1.4.1.311.54.1.2 \
    key_usage="" \
    ext_key_usage=""

# Set our policy for signing certs
vault policy write "rdp-policy" -<<EOF
path "pki/sign/rdp-cert" {
  capabilities = ["read", "update"]
}
EOF

# Setup AWS authentication. This IAM role needs to be added to every host that
# will be a Vault Client
vault auth enable aws

vault write auth/aws/role/rdp-issue-iam \
              auth_type=iam \
              bound_iam_principal_arn=arn:aws:iam::040224243460:role/${role_name} \
              policies=rdp-policy max_ttl=500h

vault write auth/aws/config/client iam_server_id_header_value=vault-demo.infra.fluffycloudsandlines.blog

# Login to check all is working
vault login -method=aws header_value=vault-demo.infra.fluffycloudsandlines.blog role=rdp-issue-iam 

# Publish the Root CA to nginx root
cp /opt/vault/tls/ca.crt.pem /usr/share/nginx/html/ca.cer
chmod a+r /usr/share/nginx/html/ca.cer
systemctl enable nginx
systemctl start nginx
