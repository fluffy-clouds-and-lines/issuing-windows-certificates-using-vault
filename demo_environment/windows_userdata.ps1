<powershell>
# Performance tweak for Invoke-WebRequest (iwr)
$ProgressPreference = 'SilentlyContinue'
iwr https://releases.hashicorp.com/vault/1.4.3/vault_1.4.3_windows_amd64.zip -outfile .\vault.zip
Expand-Archive -Path '.\vault.zip' -DestinationPath "$env:SYSTEMROOT\"
rm .\vault.zip

# Setup system env variables
[System.Environment]::SetEnvironmentVariable('VAULT_ADDR', 'https://vault-demo.infra.fluffycloudsandlines.blog:8200', [System.EnvironmentVariableTarget]::Machine)
[System.Environment]::SetEnvironmentVariable('VAULT_FORMAT', 'json', [System.EnvironmentVariableTarget]::Machine)

# Add vault server to hosts file
Add-Content -Path C:\Windows\system32\drivers\etc\hosts -Value "${vault_ip} vault-demo.infra.fluffycloudsandlines.blog"

# Get the Root CA Certificate and trust it
iwr http://${vault_ip}/ca.cer -outfile .\ca.crt
Import-Certificate -FilePath .\ca.crt -CertStoreLocation Cert:\LocalMachine\Root
Set-ItemProperty -Path HKLM:\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters -Name 'NV Domain' -Value 'win.infra.fluffycloudsandlines.blog'

# Setup the rdp cert client working dir and download scripts
New-Item -ItemType Directory -Path "$env:SYSTEMROOT\vault_rdp_sign" -Force

iwr https://gitlab.com/fluffy-clouds-and-lines/issuing-windows-certificates-using-vault/-/raw/master/client-scripts/request_rdp_cert.ps1 -OutFile "$env:SYSTEMROOT\vault_rdp_sign\request_rdp_cert.ps1"
iwr https://gitlab.com/fluffy-clouds-and-lines/issuing-windows-certificates-using-vault/-/raw/master/client-scripts/rdp-certificate.ini.tmpl -OutFile "$env:SYSTEMROOT\vault_rdp_sign\rdp-certificate.ini.tmpl"

# Schedule the task to run at boot
$task_trigger = New-ScheduledTaskTrigger -AtStartup 
$task_user = "NT AUTHORITY\SYSTEM"
$task_action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument "$env:SYSTEMROOT\vault_rdp_sign\request_rdp_cert.ps1"    
Register-ScheduledTask -TaskName "EnrolRDPCertFromVault" -Trigger $task_trigger -User $task_user -Action $task_action -RunLevel Highest –Force 

# Then reboot to trigger the script
Start-Sleep -Seconds 60
Restart-Computer -Force
</powershell>